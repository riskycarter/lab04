import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsViewComponent } from '../view/students.view.component';
import { StudentsAddComponent } from '../add/students.add.component';
import { StudentsComponent } from '../list/students.component';

const routes: Routes = [
  { path: 'view', component: StudentsViewComponent },
  { path: 'add', component: StudentsAddComponent },
  { path: 'list', component: StudentsComponent },
  {path: 'detail/:id', component: StudentsViewComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
