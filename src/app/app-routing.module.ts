import { NgModule } from '@angular/core';
import { RouterModule, Routes, Route } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';

const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '/list',
        pathMatch: 'full'
    },
    { path: '**', component: FileNotFoundComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}